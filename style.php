<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Explore the World with RazaviImran</title>


<!--  Bootstrap css file  -->
<link rel="stylesheet" href="./css/bootstrap.min.css">

<!--  font awesome icons  -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">


<!--  Magnific Popup css file  -->
<link rel="stylesheet" href="./vendor/Magnific-Popup/dist/magnific-popup.css">


<!--  Owl-carousel css file  -->
<link rel="stylesheet" href="./vendor/owl-carousel/css/owl.carousel.min.css">
<link rel="stylesheet" href="./vendor/owl-carousel/css/owl.theme.default.min.css">


<!--  custom css file  -->
<link rel="stylesheet" href="./css/style.css">

<!--  Responsive css file  -->
<link rel="stylesheet" href="./css/responsive.css">